import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

public class Indexer {
    private final String filesDir;
    private final String[] fileTypes;
    private final Config config;

    public Indexer(Config config, String filesDir, String[] fileTypes) throws FileNotFoundException {
        this.config = config;
        this.filesDir = filesDir;
        this.fileTypes = fileTypes;
        config.loadData();
    }

    public static void main(String[] args) {
        CLI.call(args, CLI.Component.INDEXER);
    }

    public static void index(Config config, String filesDir, String[] fileTypes) {
        try {
            new Indexer(config, filesDir, fileTypes).index();
        } catch (IOException e) {
            System.out.println("[Index] Errors encountered: " + e.getMessage());
        }
    }

    private void index() throws IOException {

        FileUtils.deleteDirectory(new File(config.getIndexDir()));
        Directory directory = FSDirectory.open(config.getIdx_files());
        IndexWriterConfig indexConfig = new IndexWriterConfig(config.getAnalyzer());
        // https://stackoverflow.com/questions/1844688/how-to-read-all-files-in-a-folder-from-java
        try (Stream<Path> paths = Files.walk(Paths.get(filesDir))) {
            try (IndexWriter iwriter = new IndexWriter(directory, indexConfig)) {
                paths.filter(Files::isRegularFile)
                        .map(Path::toString)
                        .filter(path -> Arrays.stream(fileTypes).anyMatch(path.toLowerCase()::endsWith))
                        .map(path -> Pair.of(path, parseContent(path)))
                        .forEach(pair -> {
                            String path = pair.getLeft();
                            System.out.println("Parsing " + path + "...");
                            String text = pair.getRight();
                            Document doc = new Document();
                            doc.add(new StringField("path", path, Field.Store.YES));
                            doc.add(new TextField("contents", text, Field.Store.YES));
                            try {
                                iwriter.addDocument(doc);
                            } catch (IOException e) {
                                System.out.println("[Index][Add doc] Errors encountered: " + e.getMessage());
                            }
                        });
            }
        }
    }

    private String parseContent(String file) {
        // https://tika.apache.org/1.24.1/examples.html
        Tika tika = new Tika();
        try {
            return tika.parseToString(Paths.get(file));
        } catch (IOException | TikaException e) {
            System.out.println("[Index][Tika] Errors encountered: " + e.getMessage());
            return "";
        }
    }
}
