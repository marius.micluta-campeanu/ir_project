import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class Searcher {
    private final Config config;
    private final String queryFile;
    private final int max_results;

    public Searcher(Config config, String queryFile, int max_results) throws FileNotFoundException {
        this.config = config;
        config.loadData();
        this.queryFile = queryFile;
        this.max_results = max_results;
    }

    public static void main(String[] args) {
        CLI.call(args, CLI.Component.SEARCHER);
    }

    public static void search(Config config, String queryFile, int max_results) {
        try {
            new Searcher(config, queryFile, max_results).search();
        } catch (IOException e) {
            System.out.println("[Search] Errors encountered: " + e.getMessage());
        }
    }

    private void search() throws IOException {
        Directory directory = FSDirectory.open(config.getIdx_files());
        DirectoryReader ireader = DirectoryReader.open(directory);
        IndexSearcher isearcher = new IndexSearcher(ireader);
        BufferedReader reader = new BufferedReader(new FileReader(new File(queryFile)));
        QueryParser parser = new QueryParser("contents", config.getAnalyzer());
        Scanner scanner = new Scanner(System.in);
        Iterator<String> iter = reader.lines().iterator();
        while (iter.hasNext()) {
            String line = iter.next();
            if (line.isEmpty() || line.startsWith("#")) {
                continue;
            }
            Query query;
            try {
                System.out.println("Query: " + line);
                query = parser.parse(line);
                ScoreDoc[] hits = isearcher.search(query, max_results).scoreDocs;
                for (ScoreDoc hit : hits) {
                    Document hitDoc = isearcher.doc(hit.doc);
                    System.out.println("--> " + hitDoc.get("path") + " " + hit.score);
                }
                if (hits.length == 0) {
                    System.out.println("No results!");
                }
            } catch (IOException | ParseException e) {
                System.out.println("Error while processing line: " + String.join("\n", Arrays.copyOfRange(e.getMessage().split("\n"), 0, 4)));
            }
            if (config.isInteractive()) {
                System.out.println("Continue? y/n");
                String opt = scanner.next();
                if (opt.toLowerCase().contains("n"))
                    break;
            }
        }
        scanner.close();
        reader.close();
        ireader.close();
        directory.close();
    }
}
