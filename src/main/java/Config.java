import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Config {

    private final String indexDir;
    private final String swFile;
    private final Path idx_files;
    private final boolean interactive;
    private final boolean no_icu;
    private Analyzer analyzer;

    public Config(String indexDir, String swFile, boolean interactive, boolean no_icu) {
        this.indexDir = indexDir;
        this.swFile = swFile;
        this.idx_files = Paths.get(indexDir);
        this.interactive = interactive;
        this.no_icu = no_icu;
    }

    public void loadData() throws FileNotFoundException {
        final int SW_COUNT = 300;  // preallocate the array

        ArrayList<String> stopwordList = new ArrayList<>();
        stopwordList.ensureCapacity(SW_COUNT);
        BufferedReader in = new BufferedReader(new FileReader(swFile));
        in.lines().forEach(stopwordList::add);

        CharArraySet stopwords = new CharArraySet(stopwordList, false);
        analyzer = new RoAnalyzer(stopwords, no_icu);
    }

    public Analyzer getAnalyzer() {
        return analyzer;
    }

    public String getIndexDir() {
        return indexDir;
    }

    public Path getIdx_files() {
        return idx_files;
    }

    public boolean isInteractive() {
        return interactive;
    }
}
