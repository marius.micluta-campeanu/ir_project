import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.StopwordAnalyzerBase;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.icu.ICUFoldingFilter;
import org.apache.lucene.analysis.icu.segmentation.ICUTokenizer;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.miscellaneous.SetKeywordMarkerFilter;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.tartarus.snowball.ext.RomanianStemmer;

import java.io.IOException;

// https://github.com/apache/lucene-solr/blob/master/lucene/analysis/common/src/java/org/apache/lucene/analysis/ro/RomanianAnalyzer.java
public final class RoAnalyzer extends StopwordAnalyzerBase {
    /**
     * File containing default Romanian stopwords.
     */
    public final static String DEFAULT_STOPWORD_FILE = "stopwords.txt";
    /**
     * The comment character in the stopwords file.
     * All lines prefixed with this will be ignored.
     */
    private static final String STOPWORDS_COMMENT = "#";
    private final CharArraySet stemExclusionSet;
    private boolean no_icu = false;

    /**
     * Builds an analyzer with the default stop words: {@link #DEFAULT_STOPWORD_FILE}.
     */
    public RoAnalyzer() {
        this(DefaultSetHolder.DEFAULT_STOP_SET);
    }

    /**
     * Builds an analyzer with the given stop words.
     *
     * @param stopwords a stopword set
     */
    public RoAnalyzer(CharArraySet stopwords) {
        this(stopwords, CharArraySet.EMPTY_SET);
    }

    /**
     * Builds an analyzer with the given stop words. If a non-empty stem exclusion set is
     * provided this analyzer will add a {@link SetKeywordMarkerFilter} before
     * stemming.
     *
     * @param stopwords        a stopword set
     * @param stemExclusionSet a set of terms not to be stemmed
     */
    public RoAnalyzer(CharArraySet stopwords, CharArraySet stemExclusionSet) {
        super(stopwords);
        this.stemExclusionSet = CharArraySet.unmodifiableSet(CharArraySet.copy(stemExclusionSet));
    }

    public RoAnalyzer(CharArraySet stopwords, boolean no_icu) {
        this(stopwords);
        this.no_icu = no_icu;
    }

    /**
     * Returns an unmodifiable instance of the default stop words set.
     *
     * @return default stop words set.
     */
    public static CharArraySet getDefaultStopSet() {
        return DefaultSetHolder.DEFAULT_STOP_SET;
    }

    /**
     * Creates a
     * {@link TokenStreamComponents}
     * which tokenizes all the text in the provided Reader.
     *
     * @return A
     * {@link TokenStreamComponents}
     * built from a Tokenizer filtered with
     * lower case and diacritics removal, {@link StopFilter}
     * , {@link SetKeywordMarkerFilter} if a stem exclusion set is
     * provided and {@link SnowballFilter}.
     */
    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        final Tokenizer tokenizer;
        TokenStream result;
        if (no_icu) {
            tokenizer = new StandardTokenizer();
            result = new LowerCaseFilter(tokenizer);
            result = new ASCIIFoldingFilter(result);
        } else {
            // https://lucene.apache.org/core/8_7_0/analyzers-icu/index.html
            tokenizer = new ICUTokenizer();
            // case folding is also performed by the following filter according to documentation
            result = new ICUFoldingFilter(tokenizer);
        }
        result = new StopFilter(result, stopwords);
        if (!stemExclusionSet.isEmpty())
            result = new SetKeywordMarkerFilter(result, stemExclusionSet);
        result = new SnowballFilter(result, new RomanianStemmer());
        return new TokenStreamComponents(tokenizer, result);
    }

//    // not needed
//    @Override
//    protected TokenStream normalize(String fieldName, TokenStream in) {
//        if(no_icu)
//            return new SnowballFilter(new LowerCaseFilter(in), new RomanianStemmer());
//        else
//            return new SnowballFilter(new ICUFoldingFilter(in), new RomanianStemmer());
//    }

    /**
     * Atomically loads the DEFAULT_STOP_SET in a lazy fashion once the outer class
     * accesses the static final set the first time.;
     */
    private static class DefaultSetHolder {
        static final CharArraySet DEFAULT_STOP_SET;

        static {
            try {
                DEFAULT_STOP_SET = loadStopwordSet(false, RoAnalyzer.class,
                        DEFAULT_STOPWORD_FILE, STOPWORDS_COMMENT);
            } catch (IOException ex) {
                // default set should always be present as it is part of the
                // distribution (JAR)
                throw new RuntimeException("Unable to load default stopword set");
            }
        }
    }
}
