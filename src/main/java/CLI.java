import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.util.Arrays;

public class CLI {
    public static void main(String[] args) {
        CLI.call(args, Component.CLI);
    }

    public static void call(String[] args, Component component) {
        Options options = new Options();
        OptionGroup group = new OptionGroup();
        if (component == Component.CLI) {
            group.setRequired(true);
            group.addOption(new Option("run_indexer", false, "Index files in `files_dir` and store the index in `index_dir`"));
            group.addOption(new Option("run_searcher", false, "Search files using `query_file` using the index in `index_dir`"));
            options.addOptionGroup(group);
        }
        Option idxOption = new Option("index_dir", true, "Lucene indexes will reside in this directory; contents will be ERASED when indexing");
        idxOption.setRequired(true);
        options.addOption(idxOption);
        options.addOption("sw_file", true, "Stopwords file path; default value: `ro_stopwords.txt`");
        options.addOption("no_icu", false, "Do not use ICU for diacritics removal; by default, ICU is used");
        options.addOption("h", "help", false, "Show usage and exit");
        if (component != Component.SEARCHER) {
            options.addOption("files_dir", true, "Files from this directory will be indexed; default value: `files`");
            Option ext = Option
                    .builder("ext")
                    .hasArgs()
                    .desc("Filter files in `files_dir` by `ext` extensions, separated by colons; default value: `.txt:.doc:.docx:.pdf`")
                    .valueSeparator(':')
                    .build();
            options.addOption(ext);
        }
        if (component != Component.INDEXER) {
            options.addOption("query_file", true, "Query file path; default value: `query.txt`");
            options.addOption("interactive", false, "Ask to continue or stop after each query");
            options.addOption("max_results", true, "Maximum number of documents to be returned; default value: `10`");
        }

        CommandLineParser parser = new DefaultParser();
        HelpFormatter help = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("h") || helpRequested(args)) {
                help.printHelp(component.toString(), options);
                return;
            }
            if (component == Component.CLI) {
                String selectedComponent = group.getSelected();
                switch (selectedComponent) {
                    case "run_indexer":
                        component = Component.INDEXER;
                        break;
                    case "run_searcher":
                        component = Component.SEARCHER;
                        break;
                    default:
                        throw new ParseException("Unknown component: " + selectedComponent);
                }
            }
        } catch (ParseException e) {
            // ignore errors when help is requested
            System.out.println(Arrays.toString(args));
            if (!helpRequested(args)) {
                System.out.println(e.getMessage());
            }
            help.printHelp(component.toString(), options);
            return;
        }

        String indexDir = cmd.getOptionValue("index_dir");
        String filesDir = cmd.getOptionValue("files_dir", "files");
        String swFile = cmd.getOptionValue("sw_file", "ro_stopwords.txt");
        String queryFile = cmd.getOptionValue("query_file", "query.txt");
        String[] ext = cmd.getOptionValues("ext");
        if (ext == null || ext.length == 0) {
            ext = new String[]{".txt", ".doc", ".docx", ".pdf"};
        }
        boolean interactive = cmd.hasOption("interactive");
        boolean no_icu = cmd.hasOption("no_icu");
        int max_results;
        try {
            max_results = Integer.parseInt(cmd.getOptionValue("max_results", "10"));
        } catch (NumberFormatException _e) {
            max_results = 10;
        }

        Config config = new Config(indexDir, swFile, interactive, no_icu);
        switch (component) {
            case INDEXER:
                Indexer.index(config, filesDir, ext);
                break;
            case SEARCHER:
                Searcher.search(config, queryFile, max_results);
        }
    }

    private static boolean helpRequested(String[] args) {
        return Arrays.stream(args).anyMatch(s -> s.toLowerCase().equals("-h") || s.toLowerCase().contains("help"));
    }

    enum Component {
        INDEXER("Indexer"), SEARCHER("Searcher"), CLI("CLI");

        private final String name;

        Component(String name) {

            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
