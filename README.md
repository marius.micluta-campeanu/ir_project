# Information Retrieval system for Romanian

Student: Micluța-Câmpeanu Marius (507)

### Contents
- Requirements
- Dependencies
- Description
- Setup
- Usage
- Help

### Requirements

- Java JDK 8 (also tested with Java 11)
- Apache Maven 3.6
- ~120 MB free space for dependencies; additional space is required for
packaging the jar(s)

### Dependencies

- Apache Lucene 8.7.0
(lucene-core, lucene-analyzers-common, lucene-queryparser)
  - Lucene Analyzers ICU 8.7.0, which depends on
    [ICU 62.1](http://site.icu-project.org/)
- Apache Tika 1.24.1 (tika-core and tika-parsers)
- Apache Commons Lang3 3.11
- Apache Commons CLI 1.4

These are also listed in `pom.xml` (`mvn dependency:tree` could also be
useful).

### Description

The goal of this project is to be able to index and search Romanian documents
while being agnostic to diacritics.

The analyzer for Romanian provided by Lucene expects the input to have
diacritics, so I inherited `StopwordAnalyzerBase` in order to change this
behavior. Inheriting directly from `RomanianAnalyzer` is not possible because
this class is `final`.

The only necessary changes to this analyzer were:
- eliminating diacritics from the input
- eliminating diacritics from the stopwords list

Eliminating diacritics from the stopwords list is straightforward with
"find and replace".

On the other hand, UTF-8 user input presents the challenge of various forms
of Romanian-like diacritics:
- ă (U+0103), â (U+00E2), î (U+00EE), ș (U+0219), ț (U+021B)
and the upper case variants (the "true" Romanian diacritics)
- ş (U+015F) and ţ (U+0163), mainly due to ISO 8859-2/Windows-1250
- ã (U+00E3) seems to be from ISO 8859-1/Windows-1252

I have seen only the above "in the wild", but there might be many others, such
as ǎ (U+01CE) (found using https://unicode.flopp.net/).

For these reasons, I chose to use ICU for Unicode normalization, since it is
a specialized library.

Since I remove diacritics, I am not concerned about collation.

The directory containing the files to be indexed is searched recursively. This
directory might also contain files that I am not interested to index, so the
first step is to filter all files that do not match the desired extensions:
`.txt`, `.doc`, `.docx` and `.pdf` (can be changed using a CLI flag).

Of course, these files might not be actual documents, so Tika will handle the
rest, determining the file type based on different heuristics. Digging into
Tika detectors and parsers seemed overkill in order to avoid indexing legit
files of other types (such as `.xml` and `.html`) disguised under one of the
extensions above. After all, we are interested in finding relevant information
regardless of the file type.

### Setup

Compile:
```
$ mvn compile
```

Then either use `java -cp classpath CLI args` (though there are many jars that
need to be included) or package everything as a fat jar:
```
$ mvn package
```

After these steps, the `target` folder will contain 3 jars: `CLI`, `Indexer`
and `Searcher`, ~90-100MB each.

**Note:** Consider deleting `Indexer` and `Searcher` executions from `pom.xml`
to speed the build time and save disk space; these are provided just to have
the components completely separate, not just by using CLI switches. 

`CLI` can be used for indexing or searching, `Indexer` can only be used for
indexing and `Searcher` can only be used for searching.

Even for `CLI`, indexing and searching are completely separate workflows. It
is the sole responsibility of the user to reindex the files if changes occur.

These entrypoints are provided for completeness and convenience, but the code
is the same for all of them (the packaging is different). There might be
better ways to package these jars, but I'm not very familiar with the Java
ecosystem.

All files must be UTF-8 encoded (or at least they should).

#### Indexing

For indexing, the following should be provided:
- index directory: path to empty or nonexistent directory where index files
will be stored; if this directory has any files, they will be ERASED; this
option has no default value to prevent data loss
- files directory: path to directory containing files to be indexed;
defaults to `files`; sample files are provided in the project root
- stopwords file: path to a file containing one stopword per line without
diacritics; defaults to `ro_stopwords.txt`, which is provided in the project
root

Files directory is intentionally not the current directory because it should
be provided by the user.

#### Searching

For searching, the following should be provided:
- index directory: path to the index directory created by Indexer component;
must exist
- query file: a text file UTF-8 encoded with 1 query per line (lines starting
with # are ignored); defaults to `query.txt`
- stopwords file: the same file used for indexing; defaults to
`ro_stopwords.txt` since arguments use the same code for both components

### Usage

Examples for Windows are below. For *nix, use forward slashes. The commands
below have `target` as the current directory.

Indexing:
```
$ java -jar CLI.jar -run_indexer -files_dir "..\files" -index_dir idx -sw_file "..\ro_stopwords.txt"
```

Searching:
```
$ java -jar CLI.jar -run_searcher -index_dir idx -sw_file "..\ro_stopwords.txt" -query_file "..\query.txt"
```

#### Misc
- `-no_icu` for both indexing and searching; do not use ICU analyzers
(option provided as fallback if ICU does not work for some reason)
- `-ext .txt:.pdf` to index only files with text and PDF extensions
- `-interactive` for searching; prompt user to continue after each query
- `max_results` for searching; limit the number of results returned
- `-h` for help, see below
- the options `-run_indexer` and `run_searcher` cannot be used together

### Help

```
$ java -jar CLI.jar -h
usage: CLI
 -ext <arg>           Filter files in `files_dir` by `ext` extensions,
                      separated by colons; default value:
                      `.txt:.doc:.docx:.pdf`
 -files_dir <arg>     Files from this directory will be indexed; default
                      value: `files`
 -h,--help            Show usage and exit
 -index_dir <arg>     Lucene indexes will reside in this directory;
                      contents will be ERASED when indexing
 -interactive         Ask to continue or stop after each query
 -max_results <arg>   Maximum number of documents to be returned; default
                      value: `10`
 -no_icu              Do not use ICU for diacritics removal; by default,
                      ICU is used
 -query_file <arg>    Query file path; default value: `query.txt`
 -run_indexer         Index files in `files_dir` and store the index in
                      `index_dir`
 -run_searcher        Search files using `query_file` using the index in
                      `index_dir`
 -sw_file <arg>       Stopwords file path; default value:
                      `ro_stopwords.txt`
```

```
$ java -jar Indexer.jar -h
usage: Indexer
 -ext <arg>           Filter files in `files_dir` by `ext` extensions,
                      separated by colons; default value:
                      `.txt:.doc:.docx:.pdf`
 -files_dir <arg>   Files from this directory will be indexed; default
                    value: `files`
 -h,--help          Show usage and exit
 -index_dir <arg>   Lucene indexes will reside in this directory; contents
                    will be ERASED when indexing
 -no_icu            Do not use ICU for diacritics removal; by default, ICU
                    is used
 -sw_file <arg>     Stopwords file path; default value: `ro_stopwords.txt`
```

```
$ java -jar Searcher.jar -h
usage: Searcher
 -h,--help            Show usage and exit
 -index_dir <arg>     Lucene indexes will reside in this directory;
                      contents will be ERASED when indexing
 -interactive         Ask to continue or stop after each query
 -max_results <arg>   Maximum number of documents to be returned; default
                      value: `10`
 -no_icu              Do not use ICU for diacritics removal; by default,
                      ICU is used
 -query_file <arg>    Query file path; default value: `query.txt`
 -sw_file <arg>       Stopwords file path; default value:
                      `ro_stopwords.txt`
```
